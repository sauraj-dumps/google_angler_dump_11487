## angler-user 8.1.0 OPM7.181205.001 5080180 release-keys
- Manufacturer: huawei
- Platform: msm8994
- Codename: angler
- Brand: google
- Flavor: angler-user
- Release Version: 8.1.0
- Id: OPM7.181205.001
- Incremental: 5080180
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: 560
- Fingerprint: google/angler/angler:8.1.0/OPM7.181205.001/5080180:user/release-keys
- OTA version: 
- Branch: angler-user-8.1.0-OPM7.181205.001-5080180-release-keys
- Repo: google_angler_dump_11487


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
